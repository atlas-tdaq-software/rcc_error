/*
 *  package A header file
 *
 *  used to study error handling
 *
 *
 *  HP Beck  17-02-1998
 */

/* ----------------------------------------------------------------- */

#ifndef _PACK_A_H_
#define _PACK_A_H_

#include "rcc_error_test.h"  // only used for the rcc_error test application

enum 
{
  EA_OK     = 0 ,        // No Error HAS to be initialized 0
  EA_ISOPEN = (P_ID_PackA << 8) + 1,
  EA_NOTOPEN,
  EA_ERROR,
  EA_BAD,
  EA_NOCODE 
};


#define EA_OK_STR         "all OK"
#define EA_ERROR_STR      "an error calling funcB occurred"
#define EA_ISOPEN_STR     "already open"
#define EA_NOTOPEN_STR    "no open performed"
#define EA_NOCODE_STR     "no such error code"
#define EA_BAD_STR        "funcA Bad"

err_type packA_open(void);
err_type packA_close(void);
err_type funcA(int);

#endif
