/*
 *  package B
 *
 *  used to study error handling
 *
 *
 *  HP Beck  17-02-1998
 */

#include <string.h>     // memcpy
#include <stdio.h>      // printf
#include "rcc_error/rcc_error.h"
#include "packB.h"
#include "packC.h"


static int packB_opened = 0;    // flag
static err_type packB_err_get(err_pack, err_str, err_str);
 
 
err_type packB_open(void)
{
  if (!packB_opened) 
  {
    packB_opened = 1;
    rcc_error_init((err_pid)P_ID_PackB, packB_err_get);
    return(RCC_ERROR_RETURN(0, EB_OK));
  } 
  else 
    return(RCC_ERROR_RETURN(0, EB_ISOPEN));
}

 
err_type packB_close(void)
{
  if (!packB_opened) 
    return(RCC_ERROR_RETURN(0, EB_NOTOPEN));
 
  packB_opened = 0;
 
  return(RCC_ERROR_RETURN(0,EB_OK));
}  


err_type funcB(int code) 
{
  err_type err;

  if (!packB_opened) 
    return(RCC_ERROR_RETURN(0, EB_NOTOPEN));

  switch(code) 
  {
    case 3:  return(RCC_ERROR_RETURN(0, EB_OK )); break;
    case 4:  return(RCC_ERROR_RETURN(0, EB_BAD)); break;
  }

  err = funcC(code);
  if (err)
    return (RCC_ERROR_RETURN(err, EB_ERROR)); 

  return(RCC_ERROR_RETURN(0, EB_OK));
}


err_type packB_err_get(err_pack err, err_str Pid_str, err_str en_str)
{

  strcpy(Pid_str, PackB_STR);

  switch ( err ) 
  {
    case EB_OK:           strcpy(en_str, EB_OK_STR);
                          break;
    case EB_ERROR:        strcpy(en_str, EB_ERROR_STR);
                          break;
    case EB_BAD:          strcpy(en_str, EB_BAD_STR);
                          break;
    case EB_ISOPEN:       strcpy(en_str, EB_ISOPEN_STR);
                          break;
    case EB_NOTOPEN:      strcpy(en_str, EB_NOTOPEN_STR);
                          break;
    case EB_NOCODE:       strcpy(en_str, EB_NOCODE_STR);
                          break;
    default:              strcpy(en_str, EB_NOCODE_STR);
                          return(RCC_ERROR_RETURN(0, EB_NOCODE));
                          break; 
  }
  return(RCC_ERROR_RETURN(0,EB_OK));
}
