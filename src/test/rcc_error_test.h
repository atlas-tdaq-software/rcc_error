/*
 * rcc_error_test.h
 *
 * header file to be used for the rcc_error_test test application
 *
 * only three dummy packages are defined here and they all NEED a
 * unique package identifier and a textual representation for them.
 *
 * Other than that, they are never used in any DAQ-unit application
 *
 * 18. Mar. 1998  HP Beck   (iom_error_test.h)
 *  7. Nov. 2001  MAJO      Adapted to RCC environment
 */

#ifndef _RCC_ERROR_TEST_H_
#define _RCC_ERROR_TEST_H_

enum
{
  P_ID_PackA = 250,
  P_ID_PackB,
  P_ID_PackC
};

#define PackA_STR     "Package A"
#define PackB_STR     "Package B"
#define PackC_STR     "Package C"

#endif
