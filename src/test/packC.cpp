/*
 *  package C
 *
 *  used to study error handling
 *
 *
 *  HP Beck  17-02-1998
 */

#include <string.h>     // memcpy
#include <stdio.h>      // print
#include "rcc_error/rcc_error.h"
#include "packC.h"

static int packC_opened = 0;    // flag
static err_type packC_err_get(err_pack, err_str, err_str);
 
 
err_type packC_open(void)
{
  if (!packC_opened) 
  {
    packC_opened = 1;
    rcc_error_init((err_pid)P_ID_PackC, packC_err_get);
    return(RCC_ERROR_RETURN(0, EC_OK) );
  } 
  else
    return(RCC_ERROR_RETURN(0, EC_ISOPEN));
}

 
err_type packC_close(void)
{
  if (!packC_opened) 
    return(RCC_ERROR_RETURN(0, EC_NOTOPEN));
  packC_opened = 0;
  return(RCC_ERROR_RETURN(0, EC_OK));
}  


err_type funcC(int code) 
{
  if (!packC_opened) 
    return(RCC_ERROR_RETURN(0, EC_NOTOPEN));

  switch(code) 
  {
    case 6:  return(RCC_ERROR_RETURN(0, EC_BAD)); break;
  }
  return(RCC_ERROR_RETURN(0, EC_OK));
}


err_type packC_err_get(err_pack err, err_str Pid_str, err_str en_str)
{
  strcpy(Pid_str, PackC_STR);
  switch (err)  
  {
    case EC_OK:           strcpy(en_str, EC_OK_STR);
                          break;
    case EC_ISOPEN:       strcpy(en_str, EC_ISOPEN_STR);
                          break;
    case EC_NOTOPEN:      strcpy(en_str, EC_NOTOPEN_STR);
                          break;
    case EC_BAD:          strcpy(en_str, EC_BAD_STR);
                          break;
    case EC_NOCODE:       strcpy(en_str, EC_NOCODE_STR);
                          break;
    default:              strcpy(en_str, EC_NOCODE_STR);
                          return(RCC_ERROR_RETURN(0, EC_NOCODE));
                          break; 
  }
  return(RCC_ERROR_RETURN(0, EC_OK));
}
