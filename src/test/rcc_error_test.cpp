#include <string.h>     /* memcpy */
#include <stdlib.h>     /* malloc, free */
#include <stdio.h>      /* printf */

#include "rcc_error/rcc_error.h"
#include "packA.h"      /* needed for recovering... */
#include "packB.h"
#include "packC.h"


err_type recover(err_type err) 
{
  switch (RCC_ERROR_MAJOR(err)) 
  {
  case  EA_NOTOPEN:     printf("recover: opening pack A\n");
                        err = packA_open();
                        break;

  case  EB_NOTOPEN:     printf("recover: opening pack B\n");
                        err = packB_open();
                        break;

  case  EC_NOTOPEN:     printf("recover: opening pack C\n");
                        err = packC_open();
                        break;
  }
  return(err);
}


int main(void)
{
  int i;
  err_type err;

  rcc_error_open();

  printf("--------------------------------------\n"
         "Calling funcA( i ) for values i=1,...6\n"
         "--------------------------------------\n\n"
         "funcA(1)                         => OK\n"
         "funcA(2)                         => funcA Bad\n"
         "funcA(3) -> funcB(3)             => OK\n"
         "funcA(4) -> funcB(4)             => funcB Bad -> funcA Error\n"
         "funcA(5) -> funcB(5) -> funcC(5) => OK\n"
         "funcA(6) -> funcB(6) -> funcC(6) => "
                "funcC Bad -> funcB Error -> funcA Error\n\n"
         "--------------------------------------\n");

  for (i = 1; i <= 6; i++) 
  {
    printf("funcA(%d)...\n", i);
    rcc_error_print(stdout, err = funcA(i));

    if (err) 
    {   // try to recover in case of error
      while (err != recover(err)) 
      {
	printf("Try again: funcA(%d)...\n", i);
	rcc_error_print(stdout, err = funcA(i));
      }
    }
    printf("\n--------------------------------------\n");
  }
  rcc_error_close();
  exit(0);
}




