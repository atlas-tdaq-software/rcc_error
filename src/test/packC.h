/*
 *  package C header file
 *
 *  used to study error handling
 *
 *
 *  HP Beck  17-02-1998
 */


#ifndef _PACK_C_H_
#define _PACK_C_H_

#include "rcc_error_test.h"  // only used for the rcc_error test application

enum 
{
  EC_OK     = 0 ,        // No Error HAS to be initialized 0
  EC_ISOPEN = (P_ID_PackC << 8) + 1,
  EC_NOTOPEN,
  EC_NOCODE,
  EC_BAD 
};


#define EC_OK_STR      "all OK"
#define EC_ISOPEN_STR  "already open"
#define EC_NOTOPEN_STR "no open performed"
#define EC_NOCODE_STR  "no such error code"
#define EC_BAD_STR     "funcC Bad"

err_type packC_open(void);
err_type packC_close(void);
err_type funcC(int);

#endif











