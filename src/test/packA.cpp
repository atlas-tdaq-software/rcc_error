/*
 *  package A
 *
 *  used to study error handling
 *
 *
 *  HP Beck  17-02-1998
 */

#include <string.h>     // memcpy
#include <stdio.h>      // printf
#include "rcc_error/rcc_error.h"
#include "packA.h"
#include "packB.h"

static int packA_opened = 0;    // flag
static err_type packA_err_get(err_pack err, err_str Pid_str, err_str en_str);
 
 
err_type packA_open(void)
{
  if (!packA_opened)  
  {
    packA_opened = 1;
    rcc_error_init((err_pid)P_ID_PackA, packA_err_get);
    return(RCC_ERROR_RETURN(0, EA_OK));
  } 
  else
    return(RCC_ERROR_RETURN(0, EA_ISOPEN));
}

 
err_type packA_close(void)
{
  if (!packA_opened) 
    return(RCC_ERROR_RETURN(0, EA_NOTOPEN));
 
  packA_opened = 0;
  return(RCC_ERROR_RETURN(0, EA_OK));
}  


err_type funcA(int code) 
{
  err_type err;

  if (!packA_opened) 
    return(RCC_ERROR_RETURN(0, EA_NOTOPEN));

  switch(code) 
  {
    case 1:  return(RCC_ERROR_RETURN(0, EA_OK )); break;
    case 2:  return(RCC_ERROR_RETURN(0, EA_BAD)); break;
  }

  err = funcB(code);
  if (err)
    return (RCC_ERROR_RETURN(err, EA_ERROR));
  return(RCC_ERROR_RETURN(0, EA_OK));
}


err_type packA_err_get(err_pack err, err_str Pid_str, err_str en_str)
{
  strcpy(Pid_str, PackA_STR);

  switch ( err ) 
  {
    case EA_OK:           strcpy(en_str, EA_OK_STR);
                          break;
    case EA_ERROR:        strcpy(en_str, EA_ERROR_STR);
                          break;
    case EA_ISOPEN:       strcpy(en_str, EA_ISOPEN_STR);
                          break;
    case EA_NOTOPEN:      strcpy(en_str, EA_NOTOPEN_STR);
                          break;
    case EA_BAD:          strcpy(en_str, EA_BAD_STR);
                          break;
    case EA_NOCODE:       strcpy(en_str, EA_NOCODE_STR);
                          break;
    default:              strcpy(en_str, EA_NOCODE_STR);
                          return(RCC_ERROR_RETURN(0, EA_NOCODE));
                          break; 
  }
  return(RCC_ERROR_RETURN(0, EA_OK));
}
