/*
 *  package B header file
 *
 *  used to study error handling
 *
 *
 *  HP Beck  17-02-1998
 */


#ifndef _PACK_B_H_
#define _PACK_B_H_

#include "rcc_error_test.h"  // only used for the rcc_error test application

enum 
{
  EB_OK     = 0 ,        // No Error HAS to be initialized 0
  EB_ISOPEN = (P_ID_PackB   << 8) + 1,
  EB_NOTOPEN,
  EB_ERROR,
  EB_BAD,
  EB_NOCODE
};

#define EB_OK_STR      "all OK"
#define EB_ERROR_STR   "an error calling funcC occurred"
#define EB_BAD_STR     "funcB Bad"
#define EB_ISOPEN_STR  "already open"
#define EB_NOTOPEN_STR "no open performed"
#define EB_NOCODE_STR  "no such error code"

err_type packB_open(void);
err_type packB_close(void);
err_type funcB(int);


#endif











